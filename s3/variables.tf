variable "bucket_name" {
  description = "Bucket name"
  default     = "platform-terraform-2025"
}

variable backend_s3 {
    description = "Backend Bucket"
    default    = "plz-build-storage"
}
